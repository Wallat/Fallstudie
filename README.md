# Gitlab Sample C++ Project

## Documentation

Trigger Pipe

Simply clone this repository in your Gitlab instance, and make sure to have a Gitlab runner defined (preferably with a docker-based executor) and you'll see your gitlab ci builds working!

## Need Help?

Need help?  Feel free to contact Farley _at_ OlinData dot com

or

http://olindata.com/contact
